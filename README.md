# Python Project Creator Bash Script

A bash script which handles the process of creating a Python3 virtual environment, switching into that environment, and installing project requirements for Linux, MacOS, Windows, and other environments. The goal is to automate these common tasks so that team members (especially those who are not as familiar with Python) can work more quickly.

Although a requirements file is included within this repo, it is currently empty, so when running the setup script as it currently is, no dependencies will be installed. To run the setup script navigate to the project directory in your terminal and execute the following command: `bash setup.sh`
